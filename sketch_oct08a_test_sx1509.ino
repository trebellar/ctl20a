#include <Wire.h> // Include the I2C library (required)
#include <SparkFunSX1509.h> // Include SX1509 library

SX1509 io; // Create an SX1509 object

// SX1509 pin definitions:
// Note: these aren't Arduino pins. They're the SX1509 I/O:
const int SX1509_LED_PIN = 15; // LED connected to 15 (source ing current)
const int SX1509_BTN_PIN = 0; // Button connected to 0 (active-low)

bool ledState = false;


int pot0 = A0;
int sensorValue = 0;

void setup() 
{

  Serial.begin(9600);
  
  pinMode(13, OUTPUT); // Use pin 13 LED as debug output
  digitalWrite(13, LOW); // Start it as low
  // Call io.begin(<I2C address>) to initialize the I/O
  // expander. It'll return 1 on success, 0 on fail.
  if (!io.begin(0x3E))
  {
    // If we failed to communicate, turn the pin 13 LED on
    printf("No comunicacion con IO");
    digitalWrite(13, HIGH);
    while (1)
      ; // And loop forever.
  }

  // Call io.pinMode(<pin>, <mode>) to set any SX1509 pin as
  // either an INPUT, OUTPUT, INPUT_PULLUP, or ANALOG_OUTPUT
  io.pinMode(SX1509_LED_PIN, OUTPUT);
  io.pinMode(SX1509_BTN_PIN, INPUT_PULLUP);

  // Blink the LED a few times before we start:
  for (int i=0; i<5; i++)
  {
    // Use io.digitalWrite(<pin>, <LOW | HIGH>) to set an
    // SX1509 pin either HIGH or LOW:
    io.digitalWrite(SX1509_LED_PIN, HIGH);
    delay(100);
    io.digitalWrite(SX1509_LED_PIN, LOW);
    delay(100);
  }

  Serial.println("Fin setup");
}

void loop()
{
  // Use io.digitalRead() to check if an SX1509 input I/O is
  // either LOW or HIGH.
  if (io.digitalRead(SX1509_BTN_PIN) == LOW)
  {
    // If the button is pressed toggle the LED:
    Serial.println("btn presionado");
    
    ledState = !ledState;
    io.digitalWrite(SX1509_LED_PIN, ledState);
    while (io.digitalRead(SX1509_BTN_PIN) == LOW)
      ; // Wait for button to release
  }


  // analogico pots
  sensorValue = analogRead(pot0);
  Serial.println(sensorValue);
  delay (1000);
  
  
}
